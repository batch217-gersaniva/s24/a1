// console.log("Hello World");


/*
3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…

*/

const numCube = Math.pow(2,3);
console.log(`The cube of 2 is ${numCube}`);


/*
5. Create a variable address with a value of an array containing details of an address.
6. Destructure the array and print out a message with the full address using Template Literals.

*/

// Array Destructuring

const address = [258, "Washington Ave NW", "California", 90011];

// Array Destructuring
const [ houseNumber, street, state, zipCode ] = address;

//Using template literal
console.log(`I live at ${houseNumber} ${street}, ${state} ${zipCode}`);


/*
7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
8. Destructure the object and print out a message with the details of the animal using Template Literals.
*/

// Array Destructuring

const animal = ["Lolong", "saltwater crocodile", 1075, 20, 3];


// Array Destructuring
const [ name, species, weight, measurementFT, measurementIn ] = animal;

//Using template literal
console.log(`${name} was a ${species} . He weighed at ${weight} kgs with a measurement of ${measurementFT} ft ${measurementIn} in.`);




/*

9. Create an array of numbers.
10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.

*/


const arrNum = [1, 2, 3, 4, 5];

// Arrow Function with loops
// pre-arrow function

// Arrow function
arrNum.forEach((num) => {
	
	console.log(`${num}`);
	
});

let  sumOfNum = (arrNum[0] + arrNum[1]);
	console.log(`${sumOfNum}`);

let reduceNumber = arrNum.reduce( (x, y) =>{
	return x + y;
});
console.log(reduceNumber);


/*
12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.

*/



class Dog {
	constructor(name, age, breed){
		this.name = "Frankie",
		this.age = 5,
		this.breed = "Miniature Dachshund"
	}
}

const theDog = new Dog();
console.log(theDog);